<?php

//ip fetch

function get_client_ip() {
  $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function get_api($url) {
  $curl = curl_init();
  
  curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
    ),
  ));
  
  $response = curl_exec($curl);
  $err = curl_error($curl);
  
  curl_close($curl);
  
  if (!$err) {
    
  
      $json = json_decode($response);
      
      return $json;	
    }
  }



//$clientip = get_client_ip();
$clientip = '49.37.41.124';
$ipdata = get_api('https://ipinfo.io/49.37.41.124?token=ae9e7936358ce2');
//$ipdata = get_api('https://ipinfo.io/'.$clientip.'?token=ae9e7936358ce2');
$weather = get_api('https://api.openweathermap.org/data/2.5/weather?q='.$ipdata->city.','.$ipdata->region.','.$ipdata->country.'&appid=765bdceeb69b2284d55c083183edaab1&units=metric');
//latlong
$cor = explode(",", $ipdata->loc);
$icon='https://openweathermap.org/img/w/'.$weather->weather[0]->icon.'.png';

?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>IP Mafia 🌎 Know your IP Address Info & Location - Som Tips</title>
  <meta name="description" content="IP Mafia 🌎 Know your IP Address Info & Location">
  <meta name="keywords" content="IP Mafia 🌎 Know your IP Address Info & Location">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.3.0/css/ol.css" type="text/css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <meta name="msvalidate.01" content="EF17473468AC80ADF58D5A892E482E85" />
  <link rel="stylesheet" href="./style.css">
  <link rel="icon" type="image/png" href="#">
  <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.3.0/build/ol.js"></script>
  
</head>
<body>
    <nav class="navbar navbar-light bg-success">
        <div class="navbar-brand mx-auto" href="./">
            <h4 class="text-white">IP Mafia</h4>
        </div>
    </nav>
        <!-- flex box -->
    <div class="container text-center" style="padding: 20px">
        <div class="page-header">
            <h2>Your Public IP Address <i class="fa fas fa-wifi"></i></h2>
            
            <h1 class="text-danger" ><?php echo $clientip; ?></h1>
            
        </div>
        <div class="row">
            
            <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="margin: auto;">
                
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_twitter"></a>
                <a class="a2a_button_whatsapp"></a>
                <a class="a2a_button_telegram"></a>
                <a class="a2a_button_email"></a>
                
            </div>
                
            
            
        </div>
    </div>
  
    <div class="container">
  <div class="row" style="padding: 20px;">
    <div class="col-sm">
    <h3 class="text-center">IP Information</h3>
    <table class="table">
  
  <tbody>
    <tr>
      <th scope="row">Company</th>
      <td><?php echo $ipdata->org ?></td>
      
    </tr>
    <tr>
      <th scope="row">City</th>
      <td><?php echo $ipdata->city ?></td>
      
    </tr>
    <tr>
      <th scope="row">Region</th>
      <td><?php echo $ipdata->region ?></td>
      
    </tr>
    <tr>
      <th scope="row">Country</th>
      <td><?php echo $ipdata->country ?></td>
      
    </tr>
    <tr>
      <th scope="row">Time Zone</th>
      <td><?php echo $ipdata->timezone ?></td>
      
    </tr>
  </tbody>
</table>

</div>
    <div class="col-sm">
    <h3 class="text-center">Weather</h3>
    <table class="table">
  
  <tbody>
    <tr>
      <th scope="row"><?php echo $weather->weather[0]->main; ?></th>
      <td><img src="<?php echo $icon; ?>"</td>
      
    </tr>
    <tr>
      <th scope="row">Temperature</th>
      <td><?php echo $weather->main->temp.' C'; ?></td>
      
    </tr>
    <tr>
      <th scope="row">Pressure</th>
      <td><?php echo $weather->main->pressure.' hPa'; ?></td>
      
    </tr>
    <tr>
      <th scope="row">Humidity</th>
      <td><?php echo $weather->main->humidity.'%'; ?></td>
      
    </tr>
    <tr>
      <th scope="row">Visibility</th>
      <td><?php echo $weather->visibility.' meter'; ?></td>
      
    </tr>
  </tbody>
  </table>
</div>
    <div class="col-sm">
    <h3 class="text-center">Map</h3>
    <div id="map" class="map">
       
    </div>

    <script type="text/javascript">
      var map = new ol.Map({
        target: 'map',
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          })
        ],
        view: new ol.View({
          center: ol.proj.fromLonLat([<?php echo $cor[1].','.$cor[0] ?>]),
          zoom: 11
        })
      });
    </script>
    </div>
    </div>
    </div>
<div class="footer">
  <p><a href="https://somtips.com/privacy-policy/">Privacy Policy</a>  |  <a href="https://somtips.com/about-us/">About Us</a>  |  <a href="https://somtips.com/contact-us/">Contact Us</a></p>
  <p>&COPY; 2020 <a href="https://somtips.com">Som Tips </a></p> 
</div>  
<script async src="https://static.addtoany.com/menu/page.js"></script>
<script src="https://kit.fontawesome.com/e420652f78.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
